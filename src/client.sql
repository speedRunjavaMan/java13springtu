select * from clients;
drop table clients;
create table clients
(
    id     serial primary key,
    last_name  varchar(30) not null,
    first_name varchar(50) not null,
    birth_date timestamp not null,
    phone varchar(30) not null,
    email varchar(30) not null,
    book_list text not null
);
