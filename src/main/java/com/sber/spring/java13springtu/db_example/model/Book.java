package com.sber.spring.java13springtu.db_example.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Setter(AccessLevel.NONE)
    private Integer id;
    private String bookTitle;
    private String author;
    private Date dateAdded;

}
