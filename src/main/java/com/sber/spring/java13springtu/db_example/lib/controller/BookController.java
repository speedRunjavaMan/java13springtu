/*
package com.sber.spring.java13springtu.db_example.lib.controller;

import com.sber.spring.java13springtu.db_example.model.Book;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class BookController {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookController(BookRepository bookRepository,
                          AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    //вернуть информацию о книге по переданному ID
    @Operation(description = "Получить запись по ID", method = "getBook")
    @RequestMapping(value = "/getBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(bookRepository.findById(id).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена")));
    }

    @Operation(description = "Добавить новую книгу", method = "addBook")
    @RequestMapping(value = "/addBook", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> addBook(@RequestBody Book newBook) {
        bookRepository.save(newBook);
        return ResponseEntity.status(HttpStatus.CREATED).body(newBook);
    }

    @Operation(description = "Обновить книгу", method = "updateBook")
    @RequestMapping(value = "/updateBook", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> updateBook(@RequestBody Book updatedBook,
                                           @RequestParam(value = "bookId") Long bookId) {
        updatedBook.setId(bookId);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookRepository.save(updatedBook));
    }

    //@RequestParam: localhost:9090/api/rest/books/deleteBook?id=1
    //@PathVariable: localhost:9090/api/rest/books/deleteBook/1
    @Operation(description = "Удалить книгу по ID", method = "delete")
    @RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "bookId") Long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Operation(description = "Добавить автора к книге", method = "addAuthor")
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
                                          @RequestParam(value = "authorId") Long authorId) {
        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
        book.getAuthors().add(author);
        return ResponseEntity.status(HttpStatus.OK).body(bookRepository.save(book));
    }
}
*/
