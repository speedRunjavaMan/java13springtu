package com.sber.spring.java13springtu.db_example.dao;


import com.sber.spring.java13springtu.db_example.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private final Connection connection;

    public UserDao(Connection connection) {
        this.connection = connection;
    }

    public void addNewUser(String ln, String fn, Date bd, String phone, String email, String bl) throws SQLException {

        PreparedStatement sqlQuery1 = connection.prepareStatement("insert into clients(last_name, first_name, birth_date, phone,  email, book_list)\nvalues(?, ?, ?, ?, ?, ?);");
        //sqlQuery.setInt(null, userid);
        sqlQuery1.setString(1, ln);
        sqlQuery1.setString(2, fn);
        sqlQuery1.setDate(3, bd);
        sqlQuery1.setString(4, phone);
        sqlQuery1.setString(5, email);
        sqlQuery1.setString(6, bl);

        sqlQuery1.execute();
    }

    public User findUserByPhone(String phone) throws SQLException {

        PreparedStatement sqlQuery = connection.prepareStatement("select book_list from clients where phone = ?");
        sqlQuery.setString(1, phone);
        ResultSet resultset1 = sqlQuery.executeQuery();
        User user = new User();
        while (resultset1.next()) {
            user.setBook_list(resultset1.getString("book_list"));
            List<String> userbooks = new ArrayList<>();
            StringBuilder currentWord = new StringBuilder();
            for (Character letter : user.getBook_list().toCharArray()) {

                if (letter.equals(' ')) {
                    if (currentWord.length() > 0) {
                        // Записываем слово в список, если оно содержит хотя бы 1 символ
                        userbooks.add(currentWord.toString());
                        currentWord = new StringBuilder();
                    }
                } else {
                    currentWord.append(letter);
                }
            }

            if (currentWord.length() > 0) {
                userbooks.add(currentWord.toString());
            }

            BookDaoBean bookDaoBean = new BookDaoBean(connection);
            for (String w : userbooks) {
                bookDaoBean.findBookByTitle(w);
            }
        }
        return user;
    }

}
