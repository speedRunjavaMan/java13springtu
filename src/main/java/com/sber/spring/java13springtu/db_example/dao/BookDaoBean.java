package com.sber.spring.java13springtu.db_example.dao;


import com.sber.spring.java13springtu.db_example.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookDaoBean {
    private final Connection connection;

    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer bookId) throws SQLException {

        PreparedStatement sqlQuery = connection.prepareStatement("select * from books2 where id = ?");
        sqlQuery.setInt(1, bookId);
        ResultSet resultset = sqlQuery.executeQuery();
        Book book = new Book();
        while (resultset.next()) {

            book.setBookTitle(resultset.getString("title"));
            book.setAuthor(resultset.getString("author"));
            book.setDateAdded(resultset.getDate("date_added"));
            System.out.println(book);

        }
        return book;
    }

    public Book findBookByTitle(String title) throws SQLException {

        PreparedStatement sqlQuery1 = connection.prepareStatement("select * from books2 where title = ?");
        sqlQuery1.setString(1, title);
        ResultSet resultset1 = sqlQuery1.executeQuery();
        Book book = new Book();
        while (resultset1.next()) {

            book.setBookTitle(resultset1.getString("title"));
            book.setAuthor(resultset1.getString("author"));
            book.setDateAdded(resultset1.getDate("date_added"));
            System.out.println(book);

        }
        return book;
    }
}
