package com.sber.spring.java13springtu.db_example.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Integer id;
    private String last_name;
    private String first_name;
    private Date birth_date;
    private String phone;
    private String email;
    private String book_list;

}
