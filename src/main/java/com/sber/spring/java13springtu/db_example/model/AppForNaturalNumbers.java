package com.sber.spring.java13springtu.db_example.model;

import java.util.Scanner;

public class AppForNaturalNumbers {
    static int m = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number for check in first app:");
        isItANaturalNumber(scanner.nextInt());
        System.out.println("Enter number for check in second app:");
        isItANaturalNumber2(scanner.nextInt());
    }

    private static void isItANaturalNumber(int n) {
        if (n > 1) {
            if (n % m != 0) {
                m++;
                isItANaturalNumber(n);
            } else printResult(m, n);
        }
    }

    private static void printResult(int m, int n) {
        if (m == n) {
            System.out.println(n + " is a natural number.");
        } else {
            System.out.println(n + " is a composite number.");
        }
    }

    private static void isItANaturalNumber2(int n) {
        boolean isPrime = true;
        int num = n;
        for (int i = 2; i <= num / 2; i++) {
            n = num % i;
            if (n == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            System.out.println(num + " is a natural number.");
        } else {
            System.out.println(num + " is a composite number.");
        }
    }
}