package com.sber.spring.java13springtu.db_example;


import com.sber.spring.java13springtu.db_example.dao.BookDaoBean;
import com.sber.spring.java13springtu.db_example.dao.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.spring.java13springtu.db_example.constants.DBConstants.*;

@Configuration
public class MyDBApplicationContext {


    @Bean
    @Scope("singleton")
    public Connection newConnection() throws SQLException {

        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB, USER, PASSWORD);

    }
    @Bean
    public BookDaoBean newBookDaoBean() throws SQLException {
        return new BookDaoBean(newConnection());
    }

    @Bean
    @Scope("prototype")
    public UserDao addNewUser() throws SQLException {
        return new UserDao(newConnection());
    }
}
