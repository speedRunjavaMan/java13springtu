package com.sber.spring.java13springtu.db_example.dao;

import com.sber.spring.java13springtu.db_example.db.DBApp;
import com.sber.spring.java13springtu.db_example.model.Book;

import java.sql.*;

public class BookDaoJDBC {
    public Book findBookById(Integer bookId) {
        try (Connection connection = DBApp.INSTANCE.getConnection()) {
            if (connection != null) {
                System.out.println("Ура, я подключился к БД!");
            } else {
                System.out.println("База данных отдыхает. Постучись позже.");
            }
            PreparedStatement sqlQuery = connection.prepareStatement("select * from books2 where id = ?");
            sqlQuery.setInt(1, bookId);
            ResultSet resultset = sqlQuery.executeQuery();
            while (resultset.next()) {
                Book book = new Book();
                book.setBookTitle(resultset.getString("title"));
                book.setAuthor(resultset.getString("author"));
                book.setDateAdded(resultset.getDate("date_added"));
                System.out.println(book);
                return book;
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

//    private Connection newConnection() throws SQLException {
//        return DriverManager.getConnection(
//                "jdbc:postgresql://localhost:5432/local_db",
//                "postgres",
//                "12345");
}

