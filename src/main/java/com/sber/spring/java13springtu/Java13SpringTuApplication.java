package com.sber.spring.java13springtu;


import com.sber.spring.java13springtu.db_example.MyDBApplicationContext;
import com.sber.spring.java13springtu.db_example.dao.BookDaoBean;
import com.sber.spring.java13springtu.db_example.dao.BookDaoJDBC;
import com.sber.spring.java13springtu.db_example.dao.UserDao;
import com.sber.spring.java13springtu.db_example.model.Book;
import com.sber.spring.java13springtu.db_example.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class Java13SpringTuApplication implements CommandLineRunner {
    private NamedParameterJdbcTemplate jdbcTemplate;
    private BookDaoBean bookDaoBean;


    private UserDao userDao;

    @Autowired
    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    public void setBookDaoBean(BookDaoBean bookDaoBean) {
        this.bookDaoBean = bookDaoBean;
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public static void main(String[] args) {

        SpringApplication.run(Java13SpringTuApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDBApplicationContext.class);
//        BookDaoBean bookDaoBean = ctx.getBean(BookDaoBean.class);
        bookDaoBean.findBookByTitle("It");
        System.out.println("******************************");
//        List<Book> books =jdbcTemplate.query("select * from books2")
        System.out.println("******************************");
//        UserDao userDao = ctx.getBean(UserDao.class);
        userDao.addNewUser("Sidorov", "Ivan", Date.valueOf("2000-01-01"), "+79065555556", "ivan@mail.ru", "Firestarter");
        User user = userDao.findUserByPhone("+79065555555");
    }
}
